# KanbanBoard App

This project is a work in progress, and was created to expand my React.js and Redux.js experience. Currently, only a default board with editable and movable cards is available. Cards cannot be moved on touch devices. The UI components are mostly complete with partial functionality. Currently, working on the ability to delete a user account. Also, working on the create a new board functionality.

## Demo

https://www.shanaodem.com/kanbanboard/

## My Roles

- Design
- User-Experience
- Front-End Development

## Tools/Skills Used

- HTML5
- SCSS
- Bootstrap
- JavaScript (ES6)
- React
- Redux
- Firebase
- Firestore
