import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { login, clearAuthError } from "../../store/actions/authActions";

class Login extends Component {
  state = {
    email: "",
    password: ""
  };

  componentDidMount = () => {
    this.props.clearAuthError();
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.login(this.state);
  };

  handleInputChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  getAuthError = error => {
    if (error) {
      return <p className="mb-0">{error}</p>;
    }
  };

  render() {
    if (this.props.auth.uid) {
      return <Redirect to="/kanbanboard/board/" />;
    }

    return (
      <div className="Auth-card card">
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <h2 className="text-center">Login</h2>
            <div className="form-group">
              <label className="Auth-label" htmlFor="email">
                Email
              </label>
              <input
                id="email"
                className="form-control"
                type="email"
                onChange={this.handleInputChange}
              />
            </div>
            <div className="form-group">
              <label className="Auth-label" htmlFor="password">
                Password
              </label>
              <input
                id="password"
                className="form-control"
                type="password"
                onChange={this.handleInputChange}
              />
            </div>
            <div className="form-group">
              <button className="btn btn-dark d-block w-100 text-white">
                Login
              </button>
              <div className="pt-4 text-danger text-center">
                {this.getAuthError(this.props.authError)}
              </div>
            </div>
          </form>
        </div>
        <div className="card-footer bg-white">
          <p className="text-center">
            No account? <Link to="/kanbanboard/signup">Sign Up</Link>
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: creds => dispatch(login(creds)),
    clearAuthError: () => dispatch(clearAuthError())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
