import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { signUp, clearAuthError } from "../../store/actions/authActions";

class SignUp extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: ""
  };

  componentDidMount = () => {
    this.props.clearAuthError();
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.signUp(this.state);
  };

  handleInputChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  getAuthError = error => {
    if (error) {
      return <p className="mb-0">{error}</p>;
    }
  };

  render() {
    if (this.props.auth.uid) {
      return <Redirect to="/kanbanboard/board/" />;
    }

    return (
      <div className="Auth-card card">
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <h2 className="text-center">Sign Up</h2>
            <fieldset className="form-group">
              <legend className="Auth-label">Full Name</legend>
              <div className="form-row">
                <div className="col">
                  <input
                    id="firstName"
                    className="form-control"
                    type="text"
                    placeholder="First name"
                    onChange={this.handleInputChange}
                    required
                  />
                </div>
                <div className="col">
                  <input
                    id="lastName"
                    className="form-control"
                    type="text"
                    placeholder="Last name"
                    onChange={this.handleInputChange}
                    required
                  />
                </div>
              </div>
            </fieldset>
            <div className="form-group">
              <label className="Auth-label" htmlFor="email">
                Email
              </label>
              <input
                id="email"
                className="form-control"
                type="email"
                onChange={this.handleInputChange}
                required
              />
            </div>
            <div className="form-group">
              <label className="Auth-label" htmlFor="password">
                Password
              </label>
              <input
                id="password"
                className="form-control"
                type="password"
                onChange={this.handleInputChange}
                required
              />
            </div>
            <div className="form-group">
              <button className="btn btn-dark d-block w-100 text-white">
                Create Account
              </button>
              <div className="pt-4 text-danger text-center">
                {this.getAuthError(this.props.authError)}
              </div>
            </div>
          </form>
        </div>
        <div className="card-footer bg-white">
          <p className="text-center">
            Already have an account? <Link to="/kanbanboard/login">Login</Link>
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signUp: newUser => dispatch(signUp(newUser)),
    clearAuthError: () => dispatch(clearAuthError())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);
