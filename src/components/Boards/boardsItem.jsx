import React from "react";
import AppLink from "../AppLink/appLink";

const BoardsItem = ({ boardName, boardId }) => {
  return (
    <React.Fragment>
      <div className="Boards-itemBoardText text-nowrap text-truncate">
        <AppLink to={"/kanbanboard/board/" + boardId} text={boardName} />
      </div>
      <button className="Boards-itemBoardBtn">&#8942;</button>
    </React.Fragment>
  );
};

export default BoardsItem;
