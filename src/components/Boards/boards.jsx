import React from "react";
import { Link } from "react-router-dom";
import Dropdown from "../Dropdown/dropdown";
import DropdownItem from "../Dropdown/dropdownItem";
import BoardsItem from "./boardsItem";

const Boards = ({ containerClasses, boards, currBoard }) => {
  const getBoardItems = board => {
    const boardProperties = Object.getOwnPropertyNames(board);
    const boardId = boardProperties[0];
    const selectedBoard = boardId === currBoard ? "true" : "false";

    return (
      <DropdownItem
        key={boardId}
        itemClasses={["Boards-itemBoard"]}
        data-currboard={selectedBoard}
      >
        <BoardsItem boardName={board[boardId]} boardId={boardId} />
      </DropdownItem>
    );
  };

  return (
    <div className={containerClasses}>
      <Dropdown
        id="userBoards"
        btnClasses={[
          "Boards-dropdownBtn btn dropdown-toggle pt-0 pb-0 font-weight-light text-white text-nowrap text-truncate"
        ]}
        btnText="Boards"
        menuClasses={["Boards-menu"]}
      >
        {boards.map(board => getBoardItems(board))}
        <DropdownItem
          itemClasses={["Boards-itemBoard Boards-itemBoard--last py-2"]}
          datacurrboard="false"
        >
          <Link
            className="btn btn-sm w-100 font-weight-light text-white bg-dark"
            to="/kanbanboard/create-board"
          >
            Create Board
          </Link>
        </DropdownItem>
      </Dropdown>
    </div>
  );
};

export default Boards;
