import React from "react";
import { connect } from "react-redux";
import { updateCard, deleteCard } from "../../store/actions/cardActions";
import { detachFromColumn } from "../../store/actions/columnActions";

const getCardInput = ({ card, updateCard, ...props }) => {
  const edit = e => {
    updateCard({ id: card.id, value: e.target.value, editing: false });
  };

  const enterPressedEdit = e => {
    if (e.key === "Enter") {
      edit(e);
    }
  };

  return (
    <textarea
      className="Card-input"
      type="text"
      autoFocus={true}
      defaultValue={card.value}
      onBlur={edit}
      onKeyPress={enterPressedEdit}
      {...props}
    />
  );
};

const getCard = ({ card, updateCard, ...props }) => {
  if (card.editing) {
    return getCardInput({ card, updateCard, ...props });
  } else {
    return (
      <p className="d-inline-block mb-0" {...props}>
        {card.value}
      </p>
    );
  }
};

const removeCard = (column, cardId, deleteCard, detachFromColumn, e) => {
  e.stopPropagation();

  detachFromColumn({ columnId: column.id, cardId });
  deleteCard({ cardId: cardId, columnId: column.id });
};

const dragCard = (cardId, e) => {
  e.dataTransfer.setData("cardId", cardId);
};

const Card = ({
  column,
  card,
  updateCard,
  deleteCard,
  detachFromColumn,
  ...props
}) => {
  return (
    <div
      className="Card"
      draggable="true"
      onDragStart={dragCard.bind(null, card.id)}
    >
      {getCard({ card, updateCard, ...props })}
      <button
        className="Card-btn"
        onClick={removeCard.bind(
          null,
          column,
          card.id,
          deleteCard,
          detachFromColumn
        )}
      >
        <span className="u-visuallyHidden">Delete Card</span>
      </button>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    updateCard: modifiedCard => {
      dispatch(updateCard(modifiedCard));
    },
    deleteCard: ({ cardId, columnId }) => {
      dispatch(deleteCard({ cardId, columnId }));
    },
    detachFromColumn: ({ columnId, cardId }) => {
      dispatch(detachFromColumn({ columnId, cardId }));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Card);
