// import React from "react";
import React, { Component } from "react";
import Boards from "../Boards/boards";
import AppLogo from "../AppLogo/appLogo";
import User from "../User/user";
import { connect } from "react-redux";

class AppHeader extends Component {
  state = {
    uid: "",
    currBoard: "",
    boards: null
  };

  componentDidMount = () => {
    if (
      this.props.auth.uid &&
      this.props.user &&
      this.props.user.currBoard &&
      this.props.user.boards
    ) {
      this.setState({
        ...this.state,
        uid: this.props.auth.uid,
        currBoard: this.props.user.currBoard,
        boards: this.props.user.boards
      });
    }
  };

  componentWillReceiveProps = nextProps => {
    if (
      nextProps.auth.uid &&
      nextProps.user &&
      nextProps.user.currBoard &&
      nextProps.user.boards &&
      (this.state.uid !== nextProps.auth.uid ||
        this.state.currBoard !== nextProps.user.currBoard ||
        this.state.boards !== nextProps.user.boards)
    ) {
      this.setState({
        ...this.state,
        uid: nextProps.auth.uid,
        currBoard: nextProps.user.currBoard,
        boards: nextProps.user.boards
      });
    }
  };

  getHref = () => {
    return this.props.auth.uid
      ? "/kanbanboard/board/" + this.state.currBoard
      : "/kanbanboard/";
  };

  render() {
    const { auth } = this.props;

    return (
      <header className="AppHeader row no-gutters bg-primary">
        {auth.uid && this.state.currBoard && this.state.boards && (
          <div className="col clearfix">
            <Boards
              containerClasses={["float-left"]}
              currBoard={this.state.currBoard}
              boards={this.state.boards}
            />
          </div>
        )}
        <div className="col">
          <AppLogo linkTo={this.getHref()} />
        </div>
        {auth.uid && (
          <div className="col clearfix">
            <User containerClasses={["float-right"]} />
          </div>
        )}
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    user: state.auth.user
  };
};

export default connect(mapStateToProps)(AppHeader);
