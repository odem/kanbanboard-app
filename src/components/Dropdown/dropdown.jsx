import React, { Component } from "react";

class Dropdown extends Component {
  state = {
    menuShown: false,
    ariaPressed: false,
    ariaExpanded: false
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside, false);
  }

  closeDropdown = () => {
    this.setState({
      menuShown: false,
      ariaPressed: false,
      ariaExpanded: false
    });
  };

  handleClickOutside = e => {
    if (this.dropdown && !this.dropdown.contains(e.target)) {
      this.closeDropdown();
    }
  };

  toggleDropdown = e => {
    this.setState(prevState => ({
      menuShown: !prevState.menuShown,
      ariaPressed: !prevState.ariaPressed,
      ariaExpanded: !prevState.ariaExpanded
    }));
  };

  render() {
    const { id, btnClasses, btnText, menuClasses, children } = this.props;
    return (
      <div
        className="dropdown"
        ref={node => {
          this.dropdown = node;
        }}
      >
        <button
          id={id}
          className={[...btnClasses].join(" ")}
          aria-pressed={this.state.ariaPressed}
          aria-expanded={this.state.ariaExpanded}
          onClick={this.toggleDropdown}
        >
          {btnText}
        </button>
        {this.state.menuShown && (
          <div
            className={[
              "dropdown-menu d-block py-0 mt-1 ml-1 shadow",
              ...menuClasses
            ].join(" ")}
            aria-labelledby={id}
            onClick={this.toggleDropdown}
          >
            <ul className="p-0 m-0">{children}</ul>
          </div>
        )}
      </div>
    );
  }
}

export default Dropdown;
