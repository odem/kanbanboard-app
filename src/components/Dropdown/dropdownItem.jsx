import React from "react";

const DropdownItem = ({ itemClasses, ...props }) => {
  return (
    <li
      className={[
        "dropdown-item-text pt-2 pb-1 pl-3 pr-3",
        ...itemClasses
      ].join(" ")}
      {...props}
    >
      {props.children}
    </li>
  );
};

export default DropdownItem;
