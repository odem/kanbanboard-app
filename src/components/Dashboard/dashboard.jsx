import React, { Component } from "react";
import { connect } from "react-redux";
import Columns from "../Columns/columns";
import { loadBoard } from "../../store/actions/boardActions";

class Dashboard extends Component {
  state = {
    currBoard: "",
    boardName: "",
    columns: [],
    cards: []
  };

  updateState = props => {
    const { user, board, loadBoard } = props;

    if (user && user.currBoard && this.state.currBoard !== user.currBoard) {
      loadBoard(user.currBoard);
      this.setState({ ...this.state, currBoard: user.currBoard });
    }

    if (
      board &&
      board.boardName &&
      board.columns &&
      board.cards &&
      (this.state.boardName !== board.boardName ||
        this.state.columns !== board.columns ||
        this.state.cards !== board.cards)
    ) {
      this.setState({
        ...this.state,
        boardName: board.boardName,
        columns: board.columns,
        cards: board.cards
      });
    }
  };

  componentDidMount = () => {
    this.updateState(this.props);
  };

  componentWillReceiveProps = nextProps => {
    this.updateState(nextProps);
  };

  render() {
    if (this.state.boardName && this.state.columns.length > 0) {
      return <Columns />;
    } else {
      return <p>Loading...</p>;
    }
  }
}

const mapStateToProps = state => {
  return {
    board: state.board,
    user: state.auth.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadBoard: board => dispatch(loadBoard(board))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
