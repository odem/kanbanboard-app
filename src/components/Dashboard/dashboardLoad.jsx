import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

class DashboardLoad extends Component {
  state = {
    currBoard: ""
  };

  updateState = props => {
    const { user } = props;

    if (user && user.currBoard && this.state.currBoard !== user.currBoard) {
      this.setState({ ...this.state, currBoard: user.currBoard });
    }
  };

  componentDidMount = () => {
    this.updateState(this.props);
  };

  componentWillReceiveProps = nextProps => {
    this.updateState(nextProps);
  };

  render() {
    if (this.state.currBoard) {
      return <Redirect to={"/kanbanboard/board/" + this.state.currBoard} />;
    } else {
      return <p>Loading...</p>;
    }
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  };
};

export default connect(mapStateToProps)(DashboardLoad);
