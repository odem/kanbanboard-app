import React from "react";

const AdminBoxInputAndBtn = ({ defaultValue, btnText, ...props }) => {
  return (
    <React.Fragment>
      <input
        className="CreateBoard-content form-control d-inline-block px-2"
        type="text"
        defaultValue={defaultValue}
      />
      <button className="CreateBoard-contentBtn btn btn-outline-secondary align-top">
        {btnText}
      </button>
    </React.Fragment>
  );
};

export default AdminBoxInputAndBtn;
