import React from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import AdminBox from "../AdminBox/adminBox";
import AdminBoxInputSxn from "../AdminBox/adminBoxInputSxn";
import AdminBoxHeadline from "../AdminBox/adminBoxHeadline";
import CreateBoardContent from "./createBoardContent";

const CreateBoard = ({ columns, auth, user }) => {
  const getCancelLinkHref = () => {
    if (user && user.currBoard) {
      return "/kanbanboard/board/" + user.currBoard;
    } else {
      return "/kanbanboard/";
    }
  };

  if (!auth.uid) return <Redirect to="/kanbanboard/login" />;

  return (
    <AdminBox
      adminBoxTitle="Create Board"
      footerChildren={
        <div className="float-right">
          <Link className="btn btn-outline-primary" to={getCancelLinkHref()}>
            Cancel
          </Link>
          <button className="btn bg-primary text-white ml-3">
            Create Board
          </button>
        </div>
      }
    >
      <AdminBoxInputSxn title="Board Name" />
      <div className="mb-2">
        <AdminBoxHeadline headline="Columns" />
        <div className="pl-2">
          <ul>
            {columns.map(column => {
              return (
                <li key={column.id} className="mb-2">
                  <CreateBoardContent
                    defaultValue={column.title}
                    btnText="Remove"
                  />
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <button className="btn btn-outline-dark ml-2">Add Column</button>
    </AdminBox>
  );
};

const mapStateToProps = state => {
  return {
    columns: state.board.columns,
    auth: state.firebase.auth,
    user: state.auth.user
  };
};

export default connect(mapStateToProps)(CreateBoard);
