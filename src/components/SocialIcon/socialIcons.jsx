import iconGlobe from "../../icons/globe.svg";
import iconLinkedin from "../../icons/li.svg";
import iconGitlab from "../../icons/gitlab.svg";
import iconMsg from "../../icons/message.svg";

const website = {
  id: "icon-website",
  linkHref: "https://www.shanaodem.com",
  linkTitle: "Visit My Portfolio Site",
  imgSrc: iconGlobe,
  imgAlt: "Visit my portfolio site."
};

const linkedin = {
  id: "icon-linkedin",
  linkHref: "https://www.linkedin.com/in/shanaodem",
  linkTitle: "Connect With Me on Linkedin",
  imgSrc: iconLinkedin,
  imgAlt: "Connect with me on Linkedin."
};

const gitlab = {
  id: "icon-gitlab",
  linkHref: "https://gitlab.com/odem",
  linkTitle: "View My Projects Code on GitLab",
  imgSrc: iconGitlab,
  imgAlt: "View the code for my projects on GitLab."
};

const message = {
  id: "icon-msg",
  linkHref: "mailto:shana@shanaodem.com",
  linkTitle: "Send Me an Email",
  imgSrc: iconMsg,
  imgAlt: "Send me an email."
};

const socialIcons = [website, linkedin, gitlab, message];

export default socialIcons;
