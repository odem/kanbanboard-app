import React from "react";

const getTargetAttr = link => {
  if (!link.includes("mailto")) {
    return "_blank";
  }
};

const getRelAttr = link => {
  if (!link.includes("mailto")) {
    return "noopener noreferrer";
  }
};

const SocialIcon = props => {
  return (
    <a
      className="d-block"
      href={props.href}
      target={getTargetAttr(props.href)}
      rel={getRelAttr(props.href)}
      title={props.linkTitle}
    >
      <img
        className="SocialIcon-img d-block"
        src={props.imgSrc}
        alt={props.imgAlt}
      />
    </a>
  );
};

export default SocialIcon;
