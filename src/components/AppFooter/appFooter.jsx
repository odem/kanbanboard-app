import React from "react";
import SocialIcon from "../SocialIcon/socialIcon";
import socialIcons from "../SocialIcon/socialIcons";

const AppFooter = () => {
  return (
    <footer className="AppFooter clearfix bg-primary">
      <div className="float-left">
        <p className="AppFooter-text">Created by Shana Odem</p>
      </div>

      <div className="clearfix">
        <ul className="float-right">
          {socialIcons.map(icon => (
            <li className="SocialIcon d-inline-block mr-1 ml-1" key={icon.id}>
              <SocialIcon
                href={icon.linkHref}
                linkTitle={icon.linkTitle}
                imgSrc={icon.imgSrc}
                imgAlt={icon.imgAlt}
              />
            </li>
          ))}
        </ul>
      </div>
    </footer>
  );
};

export default AppFooter;
