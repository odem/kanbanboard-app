import React from "react";
import { connect } from "react-redux";
import Dropdown from "../Dropdown/dropdown";
import DropdownItem from "../Dropdown/dropdownItem";
import AppLink from "../AppLink/appLink";
import { logOut } from "../../store/actions/authActions";

const insertInitials = initials => {
  if (initials) {
    return initials.toUpperCase();
  }
  return "";
};

const User = props => {
  return (
    <div className={props.containerClasses}>
      <Dropdown
        id="userAcctBtn"
        btnClasses={[
          "User-btn rounded-circle bg-secondary font-weight-light text-white text-center"
        ]}
        btnText={insertInitials(props.profile.initials)}
        menuClasses={["User-menu", "dropdown-menu-right"]}
      >
        <DropdownItem itemClasses={["User-item"]}>
          <AppLink to="/kanbanboard/settings" text="Account Settings" />
        </DropdownItem>
        <DropdownItem itemClasses={["User-item"]}>
          <div onClick={props.logOut}>
            <AppLink to="/kanbanboard/" text="Log Out" />
          </div>
        </DropdownItem>
      </Dropdown>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    profile: state.firebase.profile
  };
};
const mapDispatchToProps = dispatch => {
  return {
    logOut: () => dispatch(logOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(User);
