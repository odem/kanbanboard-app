import React from "react";

const UserContent = ({ content, btnText, btnId, btnOnClick, ariaPressed }) => {
  return (
    <React.Fragment>
      <p className="UserSettings-content d-inline-block">{content}</p>
      <button
        className="UserSettings-contentBtn btn btn-outline-secondary align-top"
        id={btnId}
        onClick={btnOnClick}
        aria-pressed={ariaPressed}
        aria-haspopup="true"
      >
        {btnText}
      </button>
    </React.Fragment>
  );
};

export default UserContent;
