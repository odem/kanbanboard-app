import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import AdminBox from "../AdminBox/adminBox";
import AdminBoxHeadline from "../AdminBox/adminBoxHeadline";
import UserContent from "./userContent";
import AdminBoxInputCol2Sxn from "../AdminBox/adminBoxInputCol2Sxn";
import AdminBoxInputSxn from "../AdminBox/adminBoxInputSxn";
import {
  userNameUpdate,
  userEmailUpdate,
  clearUpdateStatus,
  clearAuthError
} from "../../store/actions/authActions";

class UserSettings extends Component {
  state = {
    emptyInputError: false,
    fullNamePopupShown: false,
    fullNameBtnAriaPressed: false,
    emailPopupShown: false,
    emailBtnAriaPressed: false,
    deleteAcctPopupShown: false,
    deleteAcctBtnAriaPressed: false,
    demoUser: false,
    firstName: "",
    lastName: "",
    email: "",
    firstNameNew: "",
    lastNameNew: "",
    emailNew: ""
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside, false);
    this.props.clearUpdateStatus();

    if (
      this.props.profile &&
      this.props.profile.firstName &&
      this.props.profile.lastName &&
      this.props.profile.email
    ) {
      this.setState({
        ...this.state,
        firstName: this.props.profile.firstName,
        lastName: this.props.profile.lastName,
        email: this.props.profile.email,
        demoUser: this.props.profile.demo ? true : false
      });
    }
  }

  componentWillReceiveProps = nextProps => {
    if (
      nextProps.profile.firstName &&
      nextProps.profile.lastName &&
      nextProps.profile.email &&
      this.state.firstName === "" &&
      this.state.lastName === "" &&
      this.state.email === ""
    ) {
      this.setState({
        ...this.state,
        firstName: this.props.profile.firstName,
        lastName: this.props.profile.lastName,
        email: this.props.profile.email,
        demoUser: this.props.profile.demo ? true : false
      });
    }

    if (nextProps.updateStatus === "success") {
      if (this.state.emailNew) {
        this.setState({
          ...this.state,
          email: this.state.emailNew,
          emailNew: ""
        });
      }

      if (this.state.firstNameNew && this.state.lastNameNew) {
        this.setState({
          ...this.state,
          firstName: this.state.firstNameNew,
          lastName: this.state.lastNameNew,
          firstNameNew: "",
          lastNameNew: ""
        });
      }

      if (this.state.firstNameNew && !this.state.lastNameNew) {
        this.setState({
          ...this.state,
          firstName: this.state.firstNameNew,
          firstNameNew: ""
        });
      }

      if (!this.state.firstNameNew && this.state.lastNameNew) {
        this.setState({
          ...this.state,
          lastName: this.state.lastNameNew,
          lastNameNew: ""
        });
      }

      this.closeDropdown();
      nextProps.clearUpdateStatus();
    }
  };

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside, false);
  }

  closeDropdown = () => {
    this.setState({
      fullNamePopupShown: false,
      fullNameBtnAriaPressed: false,
      emailPopupShown: false,
      emailBtnAriaPressed: false,
      deleteAcctPopupShown: false,
      deleteAcctBtnAriaPressed: false
    });
  };

  handleClickOutside = e => {
    if (this.dialogBox && !this.dialogBox.contains(e.target)) {
      this.closeDropdown();
    }
  };

  handleNameUpdateSubmit = e => {
    e.preventDefault();

    if (!this.state.demoUser) {
      this.props.userNameUpdate({
        user: this.props.auth.uid,
        firstName: this.state.firstNameNew || this.state.firstName,
        lastName: this.state.lastNameNew || this.state.lastName
      });
    }
  };

  handleEmailUpdateSubmit = e => {
    e.preventDefault();

    if (!this.state.demoUser) {
      this.props.userEmailUpdate({
        user: this.props.auth.uid,
        email: this.state.emailNew
      });
    }
  };

  handleInputChange = ({ name, value }) => {
    if (!this.state.demoUser) {
      if (value) {
        const prop = name + "New";

        this.setState({
          ...this.state,
          [prop]: value,
          emptyInputError: false
        });
      } else {
        this.setState({ ...this.state, emptyInputError: true });
      }
    }
  };

  toggleFullNameDialogBox = () => {
    this.setState(prevState => ({
      fullNamePopupShown: !prevState.fullNamePopupShown,
      fullNameBtnAriaPressed: !prevState.fullNameBtnAriaPressed,
      firstNameNew: "",
      lastNameNew: ""
    }));

    this.props.clearAuthError();
    this.props.clearUpdateStatus();
  };

  toggleEmailDialogBox = () => {
    this.setState(prevState => ({
      emailPopupShown: !prevState.emailPopupShown,
      emailBtnAriaPressed: !prevState.emailBtnAriaPressed,
      emailNew: ""
    }));

    this.props.clearAuthError();
    this.props.clearUpdateStatus();
  };

  toggleDeleteAcctDialogBox = () => {
    this.setState(prevState => ({
      deleteAcctPopupShown: !prevState.deleteAcctPopupShown,
      deleteAcctBtnAriaPressed: !prevState.deleteAcctBtnAriaPressed
    }));

    this.props.clearAuthError();
  };

  render() {
    if (!this.props.auth.uid) return <Redirect to="/kanbanboard/login" />;

    return (
      <React.Fragment>
        {this.state.firstName && this.state.lastName && this.state.email && (
          <AdminBox
            adminBoxTitle="Account Settings"
            footerChildren={
              <button
                id="deleteAcct"
                className="btn btn-outline-danger"
                onClick={this.toggleDeleteAcctDialogBox}
                aria-pressed={this.state.deleteAcctBtnAriaPressed}
              >
                Delete Account
              </button>
            }
          >
            <div className="mb-3">
              <AdminBoxHeadline headline="Full Name" />
              <div className="pl-2">
                <UserContent
                  content={this.state.firstName + " " + this.state.lastName}
                  btnId="editFullName"
                  btnText="Edit"
                  btnOnClick={this.toggleFullNameDialogBox}
                  ariaPressed={this.state.fullNameBtnAriaPressed}
                />
              </div>
            </div>
            <div className="mb-3">
              <AdminBoxHeadline headline="Email" />
              <div className="pl-2">
                <UserContent
                  content={this.state.email}
                  btnId="editEmail"
                  btnText="Edit"
                  btnOnClick={this.toggleEmailDialogBox}
                  ariaPressed={this.state.emailBtnAriaPressed}
                />
              </div>
            </div>
            <Link
              className="btn btn-outline-dark"
              to="/kanbanboard/settings/password"
            >
              Change Password
            </Link>
          </AdminBox>
        )}
        {this.state.fullNamePopupShown && (
          <div className="AdminBox-dialogPopoverBg" role="dialog">
            <div
              aria-labelledby="editFullName"
              ref={node => {
                this.dialogBox = node;
              }}
            >
              <AdminBox
                adminBoxTitle="Change Name"
                uniqueBoxClasses="AdminBox-dialogPopover AdminBox-dialogPopover--center border-0"
                footerChildren={
                  <div className="float-right">
                    <button
                      className="btn btn-outline-primary"
                      onClick={this.toggleFullNameDialogBox}
                    >
                      Cancel
                    </button>
                    <button
                      className="AdminBox-dialogPopoverBtn btn text-white ml-3"
                      onClick={this.handleNameUpdateSubmit}
                      disabled={this.state.demoUser ? true : false}
                      aria-disabled={this.state.demoUser ? true : false}
                    >
                      Save
                    </button>
                  </div>
                }
              >
                <AdminBoxInputCol2Sxn
                  title="Full Name"
                  input1Id="firstName"
                  input2Id="lastName"
                  input1DefaultValue={this.state.firstName}
                  input2DefaultValue={this.state.lastName}
                  onInputChange={this.handleInputChange}
                />
                {this.state.demoUser && (
                  <p className="text-danger">
                    Cannot save demo account setting changes.
                  </p>
                )}
                {this.state.emptyInputError && (
                  <p className="text-danger">
                    Empty input fields are not allowed. Please fill in all empty
                    input fields.
                  </p>
                )}
                {this.props.authError && (
                  <p className="text-danger">{this.props.authError}</p>
                )}
              </AdminBox>
            </div>
          </div>
        )}
        {this.state.emailPopupShown && (
          <div className="AdminBox-dialogPopoverBg" role="dialog">
            <div
              aria-labelledby="editEmail"
              ref={node => {
                this.dialogBox = node;
              }}
            >
              <AdminBox
                adminBoxTitle="Change Email"
                uniqueBoxClasses="AdminBox-dialogPopover AdminBox-dialogPopover--center border-0"
                footerChildren={
                  <div className="float-right">
                    <button
                      className="btn btn-outline-primary"
                      onClick={this.toggleEmailDialogBox}
                    >
                      Cancel
                    </button>
                    <button
                      className="AdminBox-dialogPopoverBtn btn text-white ml-3"
                      onClick={this.handleEmailUpdateSubmit}
                      disabled={this.state.demoUser ? true : false}
                      aria-disabled={this.state.demoUser ? true : false}
                    >
                      Save
                    </button>
                  </div>
                }
              >
                <AdminBoxInputSxn
                  title="Email"
                  id="email"
                  defaultValue={this.state.email}
                  onInputChange={this.handleInputChange}
                />
                {this.state.demoUser && (
                  <p className="text-danger">
                    Cannot save demo account setting changes.
                  </p>
                )}
                {this.state.emptyInputError && (
                  <p className="text-danger">
                    Empty input fields are not allowed. Please fill in all empty
                    input fields.
                  </p>
                )}
                {this.props.authError && (
                  <p className="text-danger">{this.props.authError}</p>
                )}
              </AdminBox>
            </div>
          </div>
        )}
        {this.state.deleteAcctPopupShown && (
          <div className="AdminBox-dialogPopoverBg" role="dialog">
            <div
              aria-labelledby="deleteAcct"
              ref={node => {
                this.dialogBox = node;
              }}
            >
              <AdminBox
                adminBoxTitle="Delete Account"
                uniqueBoxClasses="AdminBox-dialogPopover AdminBox-dialogPopover--center border-0"
                footerChildren={
                  <div className="float-right">
                    <button
                      className="btn btn-outline-primary"
                      onClick={this.toggleDeleteAcctDialogBox}
                    >
                      Cancel
                    </button>
                    <button
                      className="AdminBox-dialogPopoverBtn btn text-white ml-3"
                      disabled={this.state.demoUser ? true : false}
                      aria-disabled={this.state.demoUser ? true : false}
                    >
                      Delete
                    </button>
                  </div>
                }
              >
                <p>Are you sure you want to delete your account?</p>
                {this.state.demoUser && (
                  <p className="text-danger">Cannot delete demo account.</p>
                )}
                {this.props.authError && (
                  <p className="text-danger">{this.props.authError}</p>
                )}
              </AdminBox>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    updateStatus: state.auth.updateStatus,
    authError: state.auth.authError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userNameUpdate: ({ user, firstName, lastName }) =>
      dispatch(userNameUpdate({ user, firstName, lastName })),

    userEmailUpdate: ({ user, email }) =>
      dispatch(userEmailUpdate({ user, email })),

    clearAuthError: () => dispatch(clearAuthError()),

    clearUpdateStatus: () => dispatch(clearUpdateStatus())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSettings);
