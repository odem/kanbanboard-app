import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import AdminBox from "../AdminBox/adminBox";
import AdminBoxInputSxn from "../AdminBox/adminBoxInputSxn";
import {
  userPasswordUpdate,
  clearUpdateStatus
} from "../../store/actions/authActions";

class ChangePassword extends Component {
  state = {
    demoUser: false,
    samePasswordError: false,
    retypePasswordError: false,
    updateSuccess: false,
    currPassword: "",
    newPassword: "",
    newPasswordConfirm: ""
  };

  componentDidMount() {
    this.props.clearUpdateStatus();

    if (this.props.profile && this.props.profile.demo) {
      this.setState({ ...this.state, demoUser: this.props.profile.demo });
    }
  }

  componentWillReceiveProps = nextProps => {
    if (
      nextProps.profile &&
      nextProps.profile.demo &&
      this.state.demoUser !== nextProps.profile.demo
    ) {
      this.setState({ ...this.state, demoUser: this.props.profile.demo });
    }

    if (nextProps.updateStatus === "success" && !this.state.updateSuccess) {
      this.setState({ ...this.state, updateSuccess: true });
    }
  };

  handleInputChange = ({ name, value }) => {
    if (!this.state.demoUser) {
      this.setState({
        ...this.state,
        [name]: value
      });
    }
  };

  handleSubmit = e => {
    e.preventDefault();

    if (!this.state.demoUser) {
      if (
        this.state.currPassword !== this.state.newPassword &&
        this.state.newPassword === this.state.newPasswordConfirm
      ) {
        this.props.userPasswordUpdate({
          currPassword: this.state.currPassword,
          newPassword: this.state.newPassword
        });

        this.setState({
          ...this.state,
          samePasswordError: false,
          retypePasswordError: false
        });
      } else if (
        this.state.currPassword === this.state.newPassword &&
        this.state.newPassword === this.state.newPasswordConfirm
      ) {
        this.setState({
          ...this.state,
          samePasswordError: true,
          retypePasswordError: false
        });
      } else if (
        this.state.currPassword !== this.state.newPassword &&
        this.state.newPassword !== this.state.newPasswordConfirm
      ) {
        this.setState({
          ...this.state,
          samePasswordError: false,
          retypePasswordError: true
        });
      } else {
        this.setState({
          ...this.state,
          samePasswordError: true,
          retypePasswordError: true
        });
      }
    }
  };

  render() {
    if (!this.props.auth.uid) return <Redirect to="/kanbanboard/login" />;

    if (this.props.auth.uid && this.state.updateSuccess) {
      return <Redirect to="/kanbanboard/settings" />;
    }

    return (
      <AdminBox
        adminBoxTitle="Change Password"
        footerChildren={
          <div className="float-right">
            <Link
              className="btn btn-outline-primary"
              to="/kanbanboard/settings"
            >
              Cancel
            </Link>
            <button
              className="AdminBox-dialogPopoverBtn btn text-white ml-3"
              onClick={this.handleSubmit}
              disabled={this.state.demoUser ? true : false}
              aria-disabled={this.state.demoUser ? true : false}
            >
              Save
            </button>
          </div>
        }
      >
        <AdminBoxInputSxn
          id="currPassword"
          title="Current Password"
          inputType="password"
          onInputChange={this.handleInputChange}
        />
        <AdminBoxInputSxn
          id="newPassword"
          title="New Password"
          inputType="password"
          onInputChange={this.handleInputChange}
        />
        <AdminBoxInputSxn
          id="newPasswordConfirm"
          title="Confirm New Password"
          inputType="password"
          onInputChange={this.handleInputChange}
        />
        {this.state.demoUser && (
          <p className="text-danger">Cannot change demo account password.</p>
        )}
        {this.state.samePasswordError && (
          <p className="text-danger">
            The new password cannot be the same as the current password.
          </p>
        )}
        {this.state.retypePasswordError && (
          <p className="text-danger">
            The new password and confirmation must match.
          </p>
        )}
        {this.props.authError && (
          <p className="text-danger">{this.props.authError}</p>
        )}
      </AdminBox>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    updateStatus: state.auth.updateStatus,
    authError: state.auth.authError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userPasswordUpdate: ({ currPassword, newPassword }) =>
      dispatch(userPasswordUpdate({ currPassword, newPassword })),

    clearUpdateStatus: () => dispatch(clearUpdateStatus())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangePassword);
