import React from "react";
import { Link } from "react-router-dom";

const AppLink = ({ to, text }) => {
  return (
    <Link className="appLink d-block text-nowrap" to={to}>
      {text}
    </Link>
  );
};

export default AppLink;
