import React from "react";
import { connect } from "react-redux";
import Card from "../Card/card";
import {
  updateCard,
  setCardIndex,
  removeCardIndex
} from "../../store/actions/cardActions";

const modifyCard = (cardId, updateCard, e) => {
  e.stopPropagation();

  updateCard({ id: cardId, editing: true });
};

const dragEnter = (columnId, cardId, setCardIndex, e) => {
  e.preventDefault();

  setCardIndex({ columnId, cardId });
};

const dragExit = (columnId, removeCardIndex, e) => {
  e.preventDefault();

  removeCardIndex({ columnId });
};

const Cards = ({
  column,
  cards,
  updateCard,
  setCardIndex,
  removeCardIndex
}) => {
  const selectedCards = [];

  cards.forEach(card => {
    let columnCardIndex = column.cards.indexOf(card.id);
    if (columnCardIndex !== -1) {
      selectedCards[columnCardIndex] = card;
    }
  });

  return (
    <ul>
      {selectedCards.map(card => (
        <li className="mb-3 border bg-light border-dark rounded" key={card.id}>
          <Card
            tabIndex="0"
            card={card}
            column={column}
            onClick={modifyCard.bind(null, card.id, updateCard)}
            onFocus={modifyCard.bind(null, card.id, updateCard)}
            onDragEnter={dragEnter.bind(null, column.id, card.id, setCardIndex)}
            onDragExit={dragExit.bind(null, column.id, removeCardIndex)}
            onDragLeave={dragExit.bind(null, column.id, removeCardIndex)}
          />
        </li>
      ))}
    </ul>
  );
};

const mapStateToProps = state => {
  return {
    cards: state.board.cards
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCard: modifiedCard => {
      dispatch(updateCard(modifiedCard));
    },
    setCardIndex: ({ columnId, cardId }) => {
      dispatch(setCardIndex({ columnId, cardId }));
    },
    removeCardIndex: ({ columnId }) => {
      dispatch(removeCardIndex({ columnId }));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cards);
