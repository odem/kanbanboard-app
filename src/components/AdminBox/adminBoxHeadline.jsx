import React from "react";

const AdminBoxHeadline = ({ headline }) => {
  return (
    <h4 className="AdminBox-headline card-title border-bottom">{headline}</h4>
  );
};

export default AdminBoxHeadline;
