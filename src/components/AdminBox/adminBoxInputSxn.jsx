import React from "react";
import AdminBoxHeadline from "./adminBoxHeadline";

const AdminBoxInputSxn = ({
  title,
  id,
  inputType,
  defaultValue,
  onInputChange
}) => {
  const handleInputChange = e => {
    onInputChange({ name: e.target.id, value: e.target.value });
  };

  return (
    <div className="AdminBox-contentSxn mb-4">
      <AdminBoxHeadline headline={title} />
      <div className="form-group pl-2">
        <input
          id={id}
          className="form-control w-100 px-2"
          type={inputType || "text"}
          defaultValue={defaultValue}
          onChange={handleInputChange}
        />
      </div>
    </div>
  );
};

export default AdminBoxInputSxn;
