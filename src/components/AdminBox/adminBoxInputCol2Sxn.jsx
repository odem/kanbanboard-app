import React from "react";
import AdminBoxHeadline from "./adminBoxHeadline";

const AdminBoxInputCol2Sxn = ({
  title,
  input1Id,
  input2Id,
  inputType,
  input1DefaultValue,
  input2DefaultValue,
  onInputChange
}) => {
  const handleInputChange = e => {
    onInputChange({ name: e.target.id, value: e.target.value });
  };

  return (
    <div className="AdminBox-contentSxn mb-4">
      <AdminBoxHeadline headline={title} />
      <div className="form-group form-row pl-2">
        <div className="col">
          <input
            id={input1Id}
            className="form-control px-2"
            type={inputType || "text"}
            defaultValue={input1DefaultValue}
            onChange={handleInputChange}
          />
        </div>
        <div className="col">
          <input
            id={input2Id}
            className="form-control px-2"
            type="text"
            defaultValue={input2DefaultValue}
            onChange={handleInputChange}
          />
        </div>
      </div>
    </div>
  );
};

export default AdminBoxInputCol2Sxn;
