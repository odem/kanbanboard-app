import React from "react";

const getClasses = uniqueClasses => {
  const boxClasses = "AdminBox card";

  if (uniqueClasses) {
    return boxClasses + " " + uniqueClasses;
  } else {
    return boxClasses;
  }
};

const AdminBox = ({
  adminBoxTitle,
  uniqueBoxClasses,
  footerChildren,
  ...props
}) => {
  return (
    <article className={getClasses(uniqueBoxClasses)}>
      <header className="card-header bg-dark text-white">
        <h3 className="AdminBox-title mb-0">{adminBoxTitle}</h3>
      </header>
      <div className="card-body">{props.children}</div>
      <footer className="card-footer bg-white clearfix">
        {footerChildren}
      </footer>
    </article>
  );
};

export default AdminBox;
