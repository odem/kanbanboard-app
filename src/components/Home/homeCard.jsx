import React from "react";

const HomeCard = ({ text, ...props }) => {
  return (
    <div className="card py-4 px-3 bg-white text-center shadow-sm">
      <div className="card-body">
        <p className="Home-cardText mb-4">{text}</p>
        {props.children}
      </div>
    </div>
  );
};

export default HomeCard;
