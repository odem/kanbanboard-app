import React from "react";
import { connect } from "react-redux";
import HomeCard from "./homeCard";
import HomeCardLink from "./homeCardLink";
import { login } from "../../store/actions/authActions";

const Home = ({ login }) => {
  const demoLogin = e => {
    e.preventDefault();
    login({
      email: "homestar@runner.com",
      password: "demo1234"
    });
  };

  return (
    <div className="Home">
      <div className="Home-introSxn row no-gutters mb-5">
        <p className="text-center">
          This project is a simplified version of the KanbanFlow app, and
          currently a work in progress. Currently, the UI components are mostly
          complete with partial functionality, and the database functionality is
          being configured.
        </p>
      </div>
      <div className="row no-gutters">
        <div className="col-12 col-md-4">
          <div className="Home-card">
            <HomeCard text="Use the demo account to view the project">
              <div onClick={demoLogin}>
                <HomeCardLink linkText="Demo" linkTo="/kanbanboard/board" />
              </div>
            </HomeCard>
          </div>
        </div>
        <div className="col-12 col-md-4">
          <div className="Home-card">
            <HomeCard text="Use your account to view the project">
              <HomeCardLink linkText="Login" linkTo="/kanbanboard/login" />
            </HomeCard>
          </div>
        </div>
        <div className="col-12 col-md-4">
          <div className="Home-card">
            <HomeCard text="Create an account to view the project">
              <HomeCardLink linkText="Sign Up" linkTo="/kanbanboard/signup" />
            </HomeCard>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    login: creds => dispatch(login(creds))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Home);
