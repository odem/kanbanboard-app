import React from "react";
import { Link } from "react-router-dom";

const HomeCardLink = ({ linkTo, linkText }) => {
  return (
    <Link className="d-block btn bg-dark text-white text-uppercase" to={linkTo}>
      {linkText}
    </Link>
  );
};

export default HomeCardLink;
