import React from "react";
import { connect } from "react-redux";
import Cards from "../Cards/cards";
import uuid from "uuid";
import {
  createCard,
  deleteCard,
  removeCardIndex
} from "../../store/actions/cardActions";
import {
  attachToColumn,
  detachFromColumn
} from "../../store/actions/columnActions";

const Column = ({
  column,
  cards,
  createCard,
  deleteCard,
  attachToColumn,
  detachFromColumn,
  removeCardIndex
}) => {
  const addCard = e => {
    e.stopPropagation();

    const cardId = uuid.v4();

    createCard({
      card: { id: cardId, value: "New Task" },
      columnId: column.id
    });
    attachToColumn({ columnId: column.id, cardId });
  };

  const dragOver = e => {
    e.preventDefault();
  };

  const dropOn = (columnId, e) => {
    const getCard = ({ cards, cardId }) => {
      for (let i = 0; i < cards.length; i++) {
        if (cards[i].id === cardId) {
          return cards[i];
        }
      }
    };

    let cardId = e.dataTransfer.getData("cardId");
    let card = getCard({ cards, cardId });

    detachFromColumn({ columnId: columnId, cardId });
    deleteCard({ cardId, columnId });

    createCard({ card, columnId });
    attachToColumn({ columnId: columnId, cardId });
    removeCardIndex({ columnId });
  };

  return (
    <article
      className="h-100"
      onDragOver={dragOver}
      onDrop={dropOn.bind(null, column.id)}
    >
      <header className="Column-header media px-3 py-2">
        <h3 className="Column-title media-body text-nowrap text-truncate text-dark">
          {column.title}
        </h3>
        <button className="Column-btn" onClick={addCard}>
          <span className="u-visuallyHidden">Add a Card</span>
        </button>
      </header>
      <div className="Column-cards px-3 py-2">
        <Cards column={column} />
      </div>
    </article>
  );
};

const mapStateToProps = state => {
  return {
    cards: state.board.cards
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createCard: ({ card, columnId }) => {
      dispatch(createCard({ card, columnId }));
    },
    deleteCard: ({ cardId, columnId }) => {
      dispatch(deleteCard({ cardId, columnId }));
    },
    attachToColumn: ({ columnId, cardId }) => {
      dispatch(attachToColumn({ columnId, cardId }));
    },
    detachFromColumn: ({ columnId, cardId }) => {
      dispatch(detachFromColumn({ columnId, cardId }));
    },
    removeCardIndex: ({ columnId }) => {
      dispatch(removeCardIndex({ columnId }));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Column);
