import React from "react";
import { connect } from "react-redux";
import Column from "../Column/column";

const Columns = ({ columns }) => {
  return (
    <ul className="Columns clearfix h-100 mb-0 bg-white">
      {columns.map(column => (
        <li className="Columns-column float-left" key={column.id}>
          <Column column={column} />
        </li>
      ))}
    </ul>
  );
};

const mapStateToProps = state => {
  return {
    columns: state.board.columns
  };
};

export default connect(mapStateToProps)(Columns);
