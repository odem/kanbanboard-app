import React from "react";
import { Link } from "react-router-dom";
import logo from "../../icons/logo-96x96.png";

const AppLogo = ({ linkTo }) => {
  return (
    <div className="AppLogo media">
      <img
        className="AppLogo-img d-none d-sm-block mr-1"
        src={logo}
        title="KanbanBoard"
        alt="KanbanBoard logo."
      />
      <div className="media-body">
        <Link
          className="AppLogo-title my-0 font-weight-normal text-secondary"
          to={linkTo}
        >
          KanbanBoard
        </Link>
      </div>
    </div>
  );
};

export default AppLogo;
