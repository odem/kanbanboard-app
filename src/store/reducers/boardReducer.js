// import uuid from "uuid";
import constants from "../../constants";

const initState = {
  boardId: null,
  boardName: "",
  columns: [],
  cards: [],
  dbNetError: false,
  dbSaveError: false
};

const boardReducer = (state = initState, action) => {
  switch (action.type) {
    case constants.LOAD_BOARD:
      return Object.assign({}, state, {
        boardId: action.boardId,
        boardName: action.board.boardName,
        columns: action.board.columns,
        cards: action.board.cards
      });

    case constants.CREATE_CARD:
      return Object.assign({}, state, {
        cards: [...state.cards, action.card]
      });

    case constants.UPDATE_CARD:
      return Object.assign({}, state, {
        cards: state.cards.map(card => {
          if (card.id === action.modifiedCard.id) {
            return Object.assign({}, card, action.modifiedCard);
          }

          return card;
        })
      });

    case constants.DELETE_CARD:
      return Object.assign({}, state, {
        cards: state.cards.filter(card => card.id !== action.cardId)
      });

    case constants.SET_CARD_INDEX:
      return Object.assign({}, state, {
        columns: state.columns.map(column => {
          if (column.id === action.columnId) {
            let cardIndex = column.cards.indexOf(action.cardId);

            if (cardIndex !== -1) {
              column = Object.assign({}, column, { cardIndex: cardIndex });
            }
          }

          return column;
        })
      });

    case constants.REMOVE_CARD_INDEX:
      return Object.assign({}, state, {
        columns: state.columns.map(column => {
          if (column.id === action.columnId) {
            let oldColumn = Object.assign({}, column);
            delete oldColumn.cardIndex;
            column = Object.assign({}, oldColumn);
          }

          return column;
        })
      });

    case constants.CREATE_COLUMN:
      return Object.assign({}, state, {
        columns: [...state.columns, action.column]
      });

    case constants.UPDATE_COLUMN:
      return Object.assign({}, state, {
        columns: state.columns.map(column => {
          if (column.id === action.modifiedColumn.id) {
            return Object.assign({}, column, action.modifiedColumn);
          }

          return column;
        })
      });

    case constants.DELETE_COLUMN:
      return Object.assign({}, state, {
        columns: state.columns.filter(column => column.id !== action.columnId)
      });

    case constants.ATTACH_TO_COLUMN:
      const columnId = action.columnId;
      const cardId = action.cardId;

      return Object.assign({}, state, {
        columns: state.columns.map(column => {
          if (column.cards.indexOf(cardId) !== -1) {
            column = {
              ...column,
              cards: column.cards.filter(card => card !== cardId)
            };
          }

          if (column.id === columnId) {
            let numCards = column.cards.length;
            let insertIndex = column.cardIndex;
            let cards = column.cards;
            let newCards = [];

            if (insertIndex === undefined) {
              newCards = [...cards, cardId];
            } else if (insertIndex === 0) {
              newCards = [cardId, ...cards];
            } else {
              let frontCards = cards.slice(0, insertIndex);
              let backCards = cards.slice(insertIndex, numCards);

              newCards = [...frontCards, cardId, ...backCards];
            }

            column = {
              ...column,
              cards: newCards
            };
          }

          return column;
        })
      });

    case constants.DETACH_FROM_COLUMN:
      return Object.assign({}, state, {
        columns: state.columns.map(column => {
          if (column.id === action.columnId) {
            return {
              ...column,
              cards: column.cards.filter(id => id !== action.cardId)
            };
          }

          return column;
        })
      });

    case constants.CLEAR_BOARD_STATE:
      return Object.assign({}, state, {
        boardId: "",
        boardName: "",
        columns: [],
        cards: []
      });

    default:
      return state;
  }
};

export default boardReducer;
