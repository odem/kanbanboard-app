import constants from "../../constants";

const initState = { user: null, authError: null, updateStatus: null };

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case constants.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        user: {
          uid: action.uid,
          boards: action.boards,
          currBoard: action.currBoard
        },
        authError: null,
        updateStatus: null
      });

    case constants.LOGIN_ERROR:
      return Object.assign({}, state, {
        user: null,
        authError: action.error.message,
        updateStatus: null
      });

    case constants.LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        user: null,
        authError: null,
        updateStatus: null
      });

    case constants.SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        user: {
          uid: action.uid,
          boards: action.boards,
          currBoard: action.currBoard
        },
        authError: null,
        updateStatus: null
      });

    case constants.SIGNUP_ERROR:
      return Object.assign({}, state, {
        user: null,
        authError: action.error.message,
        updateStatus: null
      });

    case constants.UPDATE_SUCCESS:
      return Object.assign({}, state, {
        updateStatus: "success",
        authError: null
      });

    case constants.UPDATE_FAIL:
      return Object.assign({}, state, {
        updateStatus: "fail",
        authError: action.error.message
      });

    case constants.CLEAR_UPDATE_STATUS:
      return Object.assign({}, state, { updateStatus: null });

    case constants.CLEAR_AUTH_ERROR:
      return Object.assign({}, state, { authError: null });

    default:
      return state;
  }
};

export default authReducer;
