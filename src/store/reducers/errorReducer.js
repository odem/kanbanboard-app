import constants from "../../constants";

const initState = {};

const errorReducer = (state = initState, action) => {
  switch (action.type) {
    case constants.INVALID_ARGS:
      console.log("One or more invalid arguments in: " + action.func);
      return Object.assign({}, state);

    case constants.DB_NET_ERROR:
      console.log(action.error.message);
      return Object.assign({}, state, { dbNetError: true });

    case constants.DB_SAVE_ERROR:
      console.log(action.error.message);
      return Object.assign({}, state, { dbSaveError: true });

    default:
      return state;
  }
};

export default errorReducer;
