import { combineReducers } from "redux";
import authReducer from "./authReducer";
import boardReducer from "./boardReducer";
import errorReducer from "./errorReducer";
import { firestoreReducer } from "redux-firestore";
import { firebaseReducer } from "react-redux-firebase";

const rootReducer = combineReducers({
  auth: authReducer,
  board: boardReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer,
  genError: errorReducer
});

export default rootReducer;
