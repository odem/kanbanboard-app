import constants from "../../constants";

export const loadBoard = board => {
  return (dispatch, getState, { getFirestore }) => {
    const firestore = getFirestore();

    firestore
      .collection("boards")
      .doc(board)
      .get()
      .then(doc => {
        if (doc.exists) {
          dispatch({
            type: constants.LOAD_BOARD,
            boardId: board,
            board: doc.data()
          });
        }
      })
      .catch(error => {
        dispatch({ type: constants.DB_NET_ERROR, error });
      });
  };
};
