import constants from "../../constants";

export const createColumn = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    // async call to database
    dispatch({ type: constants.CREATE_COLUMN });
  };
};

export const updateColumn = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    // async call to database
    dispatch({
      type: constants.UPDATE_COLUMN
    });
  };
};

export const deleteColumn = () => {
  // async call to database
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: constants.DELETE_COLUMN
    });
  };
};

export const attachToColumn = ({ columnId, cardId }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const boardId = getState().board.boardId;
    const boardDoc = firestore.collection("boards").doc(boardId);

    dispatch({
      type: constants.ATTACH_TO_COLUMN,
      columnId: columnId,
      cardId: cardId
    });

    boardDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          const board = doc.data();
          const columns = board.columns;

          const updatedColumns = columns.map(column => {
            if (column.cards.indexOf(cardId) !== -1) {
              column = {
                ...column,
                cards: column.cards.filter(card => card !== cardId)
              };
            }

            if (column.id === columnId) {
              const numCards = column.cards.length;
              const insertIndex = column.cardIndex;
              const cards = column.cards;
              let newCards = [];

              if (insertIndex === undefined) {
                newCards = [...cards, cardId];
              } else if (insertIndex === 0) {
                newCards = [cardId, ...cards];
              } else {
                const frontCards = cards.slice(0, insertIndex);
                const backCards = cards.slice(insertIndex, numCards);

                newCards = [...frontCards, cardId, ...backCards];
              }

              column = { ...column, cards: newCards };
            }

            return column;
          });

          boardDoc.update({ columns: updatedColumns });
        }
      })
      .then(() => {
        // removes the cardIndex property from the column
        boardDoc.get().then(doc => {
          if (doc.exists) {
            const columns = doc.data().columns;

            const updatedColumns = columns.map(column => {
              if (column.id === columnId && column.cardIndex) {
                let oldColumn = Object.assign({}, column);
                delete oldColumn.cardIndex;
                column = Object.assign({}, oldColumn);
              }

              return column;
            });

            boardDoc.update({ columns: updatedColumns });
          }
        });
      })
      .catch(error => {
        dispatch({ type: constants.DB_SAVE_ERROR, error });
      });
  };
};

export const detachFromColumn = ({ columnId, cardId }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const boardId = getState().board.boardId;
    const boardDoc = firestore.collection("boards").doc(boardId);

    dispatch({
      type: constants.DETACH_FROM_COLUMN,
      columnId: columnId,
      cardId: cardId
    });

    boardDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          const board = doc.data();
          const columns = board.columns;

          const updatedColumns = columns.map(column => {
            if (column.id === columnId) {
              column = {
                ...column,
                cards: column.cards.filter(id => id !== cardId)
              };
            }
            return column;
          });

          boardDoc.update({ columns: updatedColumns });
        }
      })
      .catch(error => {
        dispatch({ type: constants.DB_SAVE_ERROR, error });
      });
  };
};
