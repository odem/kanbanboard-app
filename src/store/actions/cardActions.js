import constants from "../../constants";
import { attachToColumn, detachFromColumn } from "./columnActions";

export const createCard = ({ card, columnId }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const boardId = getState().board.boardId;
    const boardDoc = firestore.collection("boards").doc(boardId);

    dispatch({ type: constants.CREATE_CARD, card: card });

    boardDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          boardDoc.update({ cards: firestore.FieldValue.arrayUnion(card) });
        }
      })
      .then(() => {
        attachToColumn({ cardId: card.id, columnId: columnId });
      })
      .catch(error => {
        dispatch({ type: constants.DB_SAVE_ERROR, error });
      });
  };
};

export const updateCard = modifiedCard => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const boardId = getState().board.boardId;
    const boardDoc = firestore.collection("boards").doc(boardId);

    dispatch({
      type: constants.UPDATE_CARD,
      modifiedCard: modifiedCard
    });

    boardDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          const board = doc.data();
          const cards = board.cards;
          let oldCard = null;
          let card = null;
          let cardFound = false;
          let i = 0;

          while (!cardFound && i < cards.length) {
            card = cards[i++];

            if (card.id === modifiedCard.id) {
              oldCard = card;
              cardFound = true;
            }
          }

          const updatedCard = Object.assign({}, oldCard, modifiedCard);

          boardDoc.update({
            cards: firestore.FieldValue.arrayRemove(oldCard)
          });

          boardDoc.update({
            cards: firestore.FieldValue.arrayUnion(updatedCard)
          });
        }
      })
      .catch(error => {
        dispatch({ type: constants.DB_SAVE_ERROR, error });
      });
  };
};

export const deleteCard = ({ cardId, columnId }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const boardId = getState().board.boardId;
    const boardDoc = firestore.collection("boards").doc(boardId);

    dispatch({
      type: constants.DELETE_CARD,
      cardId: cardId
    });

    boardDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          const cards = doc.data().cards;
          let cardToRemove = null;
          let card = null;
          let cardFound = false;
          let i = 0;

          while (!cardFound && i < cards.length) {
            card = cards[i++];

            if (card.id === cardId) {
              cardToRemove = card;
              cardFound = true;
            }
          }

          if (cardToRemove) {
            boardDoc.update({
              cards: firestore.FieldValue.arrayRemove(cardToRemove)
            });
          }
        }
      })
      .then(() => {
        detachFromColumn({ cardId, columnId });
      })
      .catch(error => {
        dispatch({ type: constants.DB_SAVE_ERROR, error });
      });
  };
};

export const setCardIndex = ({ columnId, cardId }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore();
    const boardId = getState().board.boardId;
    const boardDoc = firestore.collection("boards").doc(boardId);

    dispatch({
      type: constants.SET_CARD_INDEX,
      columnId: columnId,
      cardId: cardId
    });

    boardDoc.get().then(doc => {
      if (doc.exists) {
        const columns = doc.data().columns;

        const updatedColumns = columns.map(column => {
          if (column.id === columnId) {
            let cardIndex = column.cards.indexOf(cardId);

            if (cardIndex !== -1) {
              column = Object.assign({}, column, { cardIndex: cardIndex });
            }
          }

          return column;
        });

        boardDoc.update({ columns: updatedColumns });
      }
    });
  };
};

export const removeCardIndex = ({ columnId }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: constants.REMOVE_CARD_INDEX,
      columnId: columnId
    });
  };
};
