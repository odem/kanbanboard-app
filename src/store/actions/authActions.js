import constants from "../../constants";
import uuid from "uuid";

export const login = credentials => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    let userSet = false;

    if (credentials && credentials.email && credentials.password) {
      firebase
        .auth()
        .signInWithEmailAndPassword(credentials.email, credentials.password)
        .then(() => {
          firebase.auth().onAuthStateChanged(user => {
            if (!userSet && user) {
              let userId = null;
              let userBoards = null;
              let userCurrBoard = null;

              firestore
                .collection("users")
                .doc(user.uid)
                .get()
                .then(userDoc => {
                  if (userDoc.exists) {
                    const userProfile = userDoc.data();
                    userSet = true;

                    if (userProfile && userProfile.demo) {
                      const boardId = uuid.v4();
                      userId = uuid.v4();

                      if (userId && boardId) {
                        const boards = firestore.collection("boards");
                        const users = firestore.collection("users");
                        userBoards = [{ [boardId]: "Default Board" }];
                        userCurrBoard = boardId;

                        users
                          .doc("demo")
                          .get()
                          .then(demoUser => {
                            if (demoUser.exists) {
                              const newUser = users.doc(userId);

                              // Creates new user doc
                              newUser.set(demoUser.data());
                              // Creates a copy of the default board
                              boards
                                .doc("default")
                                .get()
                                .then(defaultBoard => {
                                  if (defaultBoard.exists) {
                                    boards
                                      .doc(boardId)
                                      .set(defaultBoard.data());
                                  } else {
                                    throw new Error(
                                      "Default board does not exists."
                                    );
                                  }
                                })
                                .then(() => {
                                  // Links default board copy to the newly created demo user
                                  if (userBoards && userCurrBoard) {
                                    newUser.update({
                                      boards: userBoards,
                                      currBoard: userCurrBoard
                                    });
                                  } else {
                                    throw new Error(
                                      "New demo user board properties not set."
                                    );
                                  }
                                })
                                .then(() => {
                                  // Loads user data to the user state
                                  if (userId && userBoards && userCurrBoard) {
                                    dispatch({
                                      type: constants.LOGIN_SUCCESS,
                                      uid: userId,
                                      boards: userBoards,
                                      currBoard: userCurrBoard
                                    });
                                  } else {
                                    throw new Error(
                                      "User and board properties not set."
                                    );
                                  }
                                });
                            } else {
                              throw new Error(
                                "Demo user account does not exists"
                              );
                            }
                          });
                      } else {
                        throw new Error("New demo user properties not set.");
                      }
                    } else if (
                      userProfile &&
                      userProfile.boards &&
                      userProfile.currBoard
                    ) {
                      // Get non-demo user data and loads data to the user state
                      userId = user.uid;
                      userBoards = userProfile.boards;
                      userCurrBoard = userProfile.currBoard;

                      if (userId && userBoards && userCurrBoard) {
                        dispatch({
                          type: constants.LOGIN_SUCCESS,
                          uid: userId,
                          boards: userBoards,
                          currBoard: userCurrBoard
                        });
                      }
                    } else {
                      throw new Error(
                        "User data does not contain correct properties"
                      );
                    }
                  } else {
                    throw new Error("User data does not exist.");
                  }
                });
            }
          });
        })
        .catch(error => {
          dispatch({ type: constants.LOGIN_ERROR, error });
        });
    } else {
      dispatch({ type: constants.INVALID_ARGS, func: "login(credentials)" });
    }
  };
};

export const logOut = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    const user = getState().auth.user;

    firebase
      .auth()
      .signOut()
      .then(() => {
        dispatch({ type: constants.CLEAR_BOARD_STATE });
      })
      .then(() => {
        if (user && user.uid) {
          const userDoc = firestore.collection("users").doc(user.uid);

          userDoc.get().then(doc => {
            if (doc.exists) {
              const userProfile = doc.data();

              if (userProfile && userProfile.demo && userProfile.boards) {
                const boards = firestore.collection("boards");

                userProfile.boards.forEach(boardObj => {
                  if (boardObj) {
                    for (let boardProp in boardObj) {
                      if (boardObj.hasOwnProperty(boardProp)) {
                        boards
                          .doc(boardProp)
                          .get()
                          .then(boardDoc => {
                            if (boardDoc.exists) {
                              boards.doc(boardProp).delete();
                            }
                          });
                      }
                    }
                  }
                  userDoc.delete();
                });
              }
            }
          });
        }
      })
      .then(() => {
        dispatch({ type: constants.LOGOUT_SUCCESS });
      });
  };
};

export const signUp = newUser => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    if (
      newUser &&
      newUser.email &&
      newUser.password &&
      newUser.firstName &&
      newUser.lastName
    ) {
      const firebase = getFirebase();
      const firestore = getFirestore();

      firebase
        .auth()
        .createUserWithEmailAndPassword(newUser.email, newUser.password)
        .then(response => {
          if (response && response.user && response.user.uid) {
            const userId = response.user.uid;
            const boardId = uuid.v4();
            const boards = firestore.collection("boards");

            if (userId && boardId) {
              const userBoards = [{ [boardId]: "Default Board" }];
              const userCurrBoard = boardId;

              boards
                .doc("default")
                .get()
                .then(defaultBoard => {
                  if (defaultBoard.exists) {
                    boards.doc(boardId).set(defaultBoard.data());

                    firestore
                      .collection("users")
                      .doc(response.user.uid)
                      .set({
                        firstName: newUser.firstName,
                        lastName: newUser.lastName,
                        initials: newUser.firstName[0] + newUser.lastName[0],
                        email: newUser.email,
                        boards: userBoards,
                        currBoard: userCurrBoard
                      });
                  } else {
                    throw new Error("Default board does not exists");
                  }
                })
                .then(() => {
                  dispatch({
                    type: constants.SIGNUP_SUCCESS,
                    uid: userId,
                    boards: userBoards,
                    currBoard: userCurrBoard
                  });
                });
            }
          } else {
            throw new Error("User and board properties not set");
          }
        })
        .catch(error => {
          dispatch({ type: constants.SIGNUP_ERROR, error });
        });
    } else {
      dispatch({ type: constants.INVALID_ARGS, func: "signUp(newUser)" });
    }
  };
};

export const userNameUpdate = ({ user, firstName, lastName }) => {
  return (dispatch, getState, { getFirestore }) => {
    if (user && firstName && lastName) {
      const firestore = getFirestore();
      const userDoc = firestore.collection("users").doc(user);

      userDoc
        .get()
        .then(userProfile => {
          if (userProfile.exists) {
            userDoc
              .update({ firstName: firstName, lastName: lastName })
              .then(() => {
                dispatch({ type: constants.UPDATE_SUCCESS });
              })
              .catch(error => {
                dispatch({ type: constants.UPDATE_FAIL, error });
              });
          }
        })
        .catch(error => {
          dispatch({ type: constants.UPDATE_FAIL, error });
        });
    } else {
      dispatch({ type: constants.INVALID_ARGS, func: "userNameUpdate()" });
    }
  };
};

export const userEmailUpdate = ({ user, email }) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    if (user && email) {
      const firebase = getFirebase();
      const firestore = getFirestore();

      let user = firebase.auth().currentUser;
      user
        .updateEmail(email)
        .then(() => {
          const userDoc = firestore.collection("users").doc(user.uid);

          userDoc
            .get()
            .then(userProfile => {
              if (userProfile.exists) {
                userDoc
                  .update({ email: email })
                  .then(() => {
                    dispatch({ type: constants.UPDATE_SUCCESS });
                  })
                  .catch(error => {
                    dispatch({ type: constants.UPDATE_FAIL, error });
                  });
              }
            })
            .catch(error => {
              dispatch({ type: constants.UPDATE_FAIL, error });
            });
        })
        .catch(error => {
          dispatch({ type: constants.UPDATE_FAIL, error });
        });
    } else {
      dispatchEvent({
        type: constants.INVALID_ARGS,
        func: "userEmailUpdate()"
      });
    }
  };
};

export const userPasswordUpdate = ({ currPassword, newPassword }) => {
  return (dispatch, getState, { getFirebase }) => {
    if (currPassword && newPassword) {
      const firebase = getFirebase();
      const user = firebase.auth().currentUser;

      if (user && user.email) {
        let credential = firebase.auth.EmailAuthProvider.credential(
          user.email,
          currPassword
        );

        if (credential) {
          user
            .reauthenticateAndRetrieveDataWithCredential(credential)
            .then(() => {
              user
                .updatePassword(newPassword)
                .then(() => {
                  dispatch({ type: constants.UPDATE_SUCCESS });
                })
                .catch(error => {
                  dispatch({ type: constants.UPDATE_FAIL, error });
                });
            })
            .catch(error => {
              dispatch({ type: constants.UPDATE_FAIL, error });
            });
        }
      }
    } else {
      dispatchEvent({
        type: constants.INVALID_ARGS,
        func: "userPasswordUpdate()"
      });
    }
  };
};

export const clearUpdateStatus = () => {
  return (dispatch, getState) => {
    dispatch({ type: constants.CLEAR_UPDATE_STATUS });
  };
};

export const clearAuthError = () => {
  return (dispatch, getState) => {
    dispatch({ type: constants.CLEAR_AUTH_ERROR });
  };
};
