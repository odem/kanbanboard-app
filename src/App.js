import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AppHeader from "./components/AppHeader/appHeader";
import AppFooter from "./components/AppFooter/appFooter";
import Dashboard from "./components/Dashboard/dashboard";
import DashboardLoad from "./components/Dashboard/dashboardLoad";
import CreateBoard from "./components/CreateBoard/createBoard";
import UserSettings from "./components/User/userSettings";
import ChangePassword from "./components/User/changePassword";
import Home from "./components/Home/home";
import SignUp from "./components/Auth/signUp";
import Login from "./components/Auth/login";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <AppHeader />

          <main className="AppMain p-4">
            <Switch>
              <Route exact path="/kanbanboard/" component={Home} />
              <Route path="/kanbanboard/signup" component={SignUp} />
              <Route path="/kanbanboard/login" component={Login} />
              <Route
                exact
                path="/kanbanboard/board"
                component={DashboardLoad}
              />
              <Route
                exact
                path="/kanbanboard/board/:id"
                component={Dashboard}
              />
              <Route path="/kanbanboard/create-board" component={CreateBoard} />
              <Route
                exact
                path="/kanbanboard/settings"
                component={UserSettings}
              />
              <Route
                path="/kanbanboard/settings/password"
                component={ChangePassword}
              />
            </Switch>
          </main>
          <AppFooter />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
