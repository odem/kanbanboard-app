import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyD-i4XUueMHPZ7rOAW4Nh35M6s7Bc-Ihhs",
  authDomain: "kanbanboard-e354b.firebaseapp.com",
  databaseURL: "https://kanbanboard-e354b.firebaseio.com",
  projectId: "kanbanboard-e354b",
  storageBucket: "kanbanboard-e354b.appspot.com",
  messagingSenderId: "448721913789"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;
